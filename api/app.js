const express = require('express');
const cors = require('cors');

const app = express();
const PORT = process.env.PORT || 5000;


app.use(cors());
app.use(express.json());

const logger = require('./startup/logger.js')
require('./startup/db.js')();
require('./startup/router.js')(app);

app.listen(PORT, () => logger.info(`Serveur lancé au PORT: ${PORT}`));