const { sign } = require('jsonwebtoken');
const { User } = require('../../models/user');

module.exports.authenticate = async ({ email, password }) => {
    try {
        let user = await User.findOne({ email });
        if(!user) throw new Error("This user does not exist");

        if(!user.comparePasswords(password)) throw new Error("The password is not correct");

        let token = await sign({ _id: user._id, is_staff: user.is_staff, is_teacher: user.is_teacher }, 'secret');
        return { token, is_teacher: user.is_teacher, is_staff: user.is_staff };
    } catch(ex) { throw ex; }
}