const { Category, validateCategory } = require('../../models/category');
const { isStaff } = require('../../middlewares/auth');

module.exports.categories = async () => {
    try {
        const categories = await Category.find();
        return !categories ? [] : categories;
    } catch(ex) {
        throw ex;
    }
}

module.exports.category = async (data) => {
    try {
        const category = await Category.findOne(data);
        if(!category) throw new Error("This category does not exist");
        return category;
    } catch(ex) { throw ex; }
}

module.exports.createCategory = async (data, req) => {

    await isStaff(req);

    try {
        const { error } = validateCategory(data);
        if(error) throw new Error(error.details[0].message);

        let category = new Category(data);
        category = await category.save();
        return category;
    } catch(ex) { throw ex; }
}

module.exports.deleteCategory = async (data, req) => {

    await isStaff(req);

    try {
        const category = await Category.findOneAndRemove(data);
        if(!category) throw new Error("This category does not exist");
        return category;
    } catch(ex) { throw ex; }
}