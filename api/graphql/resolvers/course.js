const { Course, validateCourse } = require('../../models/course');
const { Topic } = require('../../models/topic');
const { User } = require('../../models/user');
const { isTeacher, isStaff } = require('../../middlewares/auth');

module.exports.courses = async () => {
    try {
        const courses = await Course.find().populate('topic');
        return !courses ? [] : courses;
    } catch(ex) { throw ex; }
}

module.exports.coursesByTopic = async () => {
    try {
        const topics = await Topic.find().populate('category');
        let datas = topics.map(async (topic) => {
            const courses = await Course.find({ topic });
            const data = {
                topic,
                courses
            };
            return data;
        });
        return !datas ? [] : datas;
    } catch(ex) { throw ex; }
}

module.exports.course = async (data) => {
    try {
        const course = await Course.findOne(data).populate('topic').populate('creators');
        if(!course) throw new Error("This course does not exist");
        return course;
    } catch(ex) { throw ex; }
}

module.exports.createCourse = async (data, req) => {

    let { user } = await isTeacher(req);

    try {
        const { error } = validateCourse(data);
        if(error) throw new Error(error.details[0].message);

        let course = new Course(data);

        user = await User.findOne(user);
        course = await course.save();

        course.creators.push(user);
        user.courses.push(course);

        await user.save();
        course = await course.save();

        let topic = await Topic.findOne({ _id: data.topic });
        topic.courses.push(course);
        await topic.save();

        return course;
    } catch(ex) { throw ex; }
}

module.exports.deleteCourse = async (data, req) => {

    await isStaff(req);

    try {
        const course = await Course.findOneAndRemove(data);
        if(!course) throw new Error("This course does not exist");
        return course;
    } catch(ex) { throw ex; }
}