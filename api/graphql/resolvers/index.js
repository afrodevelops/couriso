const { createUser, deleteUser, user, users, userDatas } = require('./user');
const { createCategory, deleteCategory, categories, category } = require('./category');
const { createTopic, deleteTopic, topics, topic } = require('./topic');
const { createCourse, deleteCourse, courses, course, coursesByTopic } = require('./course');
const { authenticate } = require('./auth');

module.exports = {
    createUser, user, deleteUser, users, userDatas,
    createCategory, deleteCategory, categories, category,
    createTopic, deleteTopic, topics, topic,
    createCourse, deleteCourse, courses, course, coursesByTopic,
    authenticate
}