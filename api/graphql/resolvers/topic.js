const { Topic, validateTopic } = require('../../models/topic');
const { Category } = require('../../models/category');
const { isStaff, isTeacher } = require('../../middlewares/auth');

module.exports.topics = async () => {
    try {
        const topics = await Topic.find().populate('category');
        return !topics ? []: topics;
    }catch(ex) { throw ex; }
}

module.exports.topic = async (data) => {
    try {
        const topic = await Topic.findOne(data).populate('category');
        if(!topic) throw new Error("This topic does not exist");
        return topic;
    } catch(ex) { throw ex; }
}

module.exports.createTopic = async (data, req) => {
    await isTeacher(req);
    try {
        const { error } = validateTopic(data);
        if(error) throw new Error(error.details[0].message);

        let topic = new Topic(data);
        topic = await topic.save();

        let category = await Category.findOne({ _id: data.category });
        category.topics.push(topic);

        await category.save();

        return topic;
    } catch(ex) { throw ex; }
}

module.exports.deleteTopic = async (data, req) => {
    await isStaff(req);
    try {
        const topic = await Topic.findOneAndRemove(data);
        if(!topic) throw new Error("This topic does not exist");
        return topic;
    } catch(ex) { throw ex; }
}