const { User, validateUser } = require('../../models/user');
const { isLoggedIn } = require('../../middlewares/auth');

module.exports.createUser = async (data) => {
    try {
        const { error } = validateUser(data);
        if(error) throw new Error(error.details[0].message);

        let user = new User(data);
        user = await user.save();
        return user;
    } catch(ex) {
        throw ex;
    }
}

module.exports.deleteUser = async ({ username }) => {
    try {
        const user = await User.findOneAndRemove({ username });
        if(!user) throw new Error("THis username does not exist");
        return user;
    } catch(ex) {
        throw ex;
    }
}

module.exports.userDatas = async (_, req) => {
    try {
        let { user } = await isLoggedIn(req);
        user = await User.findOne({ _id: user._id });
        return user;
    } catch(ex) { throw ex; }
}

module.exports.user = async ({ username }) => {
    try {
        const user = await User.findOne({ username });
        if(!user) throw new Error("This username does not exit");
        return user;
    }catch(ex) {
        throw ex;
    }
}

module.exports.users = async () => {
    try {
        const users = await User.find();
        return users;
    } catch(ex) {
        throw ex;
    }
}