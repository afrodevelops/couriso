module.exports.authType = `
    type Auth {
        token: String
        is_teacher: Boolean
        is_staff: Boolean
    }
`;

module.exports.authQueries = `
    authenticate(email: String!, password: String!): Auth!
`;