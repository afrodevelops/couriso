module.exports.categoryType = `
    type Category {
        _id: ID!
        name: String!
        slug: String!
        topics: [Topic!]!
        description: String
        createdAt: DateTime!
        updatedAt: DateTime!
    }
`;

module.exports.categoryQueries = `
    categories: [Category!]!
    category(_id: ID!): Category
`;

module.exports.categoryMutations = `
    createCategory(name: String!, description: String!): Category
    deleteCategory(_id: ID!): Category
`;