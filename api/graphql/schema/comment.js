module.exports.commentType = `
    type Comment {
        user: User!
        content: String!
        createdAt: String!
        comments: [Comment!]!
    }
`;
