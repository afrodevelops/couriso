module.exports.courseType = `
    type Course {
        _id: ID!
        name: String!
        slug: String!
        description: String!
        creators: [User!]!
        students: [User!]!
        comments: [Comment!]!
        questions: [Question!]!
        sections: [Section!]!
        createdAt: DateTime!
        updatedAt: DateTime!
        topic: Topic!
    }

    type CoursesAndTopic {
        courses: [Course!]
        topic: Topic!
    }
`;

module.exports.courseMutations = `
    createCourse(name: String!, description: String, creators: [String!], topic: String!): Course
    deleteCourse(_id: ID!): Course
`;

module.exports.courseQueries = `
    courses: [Course!]
    coursesByTopic: [CoursesAndTopic!]
    course(slug: String!): Course
`;