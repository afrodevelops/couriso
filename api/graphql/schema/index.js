const { buildSchema } = require('graphql');

const { commentType } = require('./comment');
const { questionType } = require('./question');
const { sectionType } = require('./section');
const { courseType, courseQueries, courseMutations } = require('./course');
const { categoryType, categoryQueries, categoryMutations } = require('./category');
const { topicType, topicQueries, topicMutations } = require('./topic');
const { userType, userQueries, userResolvers } = require('./user');
const { authType, authQueries } = require('./auth');

module.exports = buildSchema(`

    scalar DateTime

    ${categoryType}
    ${topicType}
    ${commentType}
    ${questionType}
    ${sectionType}
    ${userType}
    ${courseType}
    ${authType}

    type Queries {
        ${userQueries}
        ${categoryQueries}
        ${topicQueries}
        ${courseQueries}
        ${authQueries}
    }

    type Mutations {
        ${userResolvers}
        ${categoryMutations}
        ${topicMutations}
        ${courseMutations}
    }

    schema {
        query: Queries
        mutation: Mutations
    }
`);