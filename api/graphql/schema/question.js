module.exports.questionType = `
    type Question {
        user: User!
        content: String!
        createdAt: String!
        answers: [Comment!]!
    }
`;