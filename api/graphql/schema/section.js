module.exports.sectionType = `
    type Section {
        _id: ID!
        title: String!
        content: String!
    }
`;