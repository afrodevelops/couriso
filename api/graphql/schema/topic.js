module.exports.topicType = `
    type Topic {
        _id: ID!
        name: String!
        slug: String!
        description: String
        createdAt: DateTime!
        updatedAt: DateTime!
        category: Category
        courses: [Course!]!
    }
`;

module.exports.topicQueries = `
    topics: [Topic!]!
    topic(_id: ID!): Topic
`;

module.exports.topicMutations = `
    createTopic(name: String!, description: String!, category: String!): Topic
    deleteTopic(_id: ID!): Topic
`;