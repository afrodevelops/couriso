module.exports.userType = `
    type User {
        _id: ID!
        username: String!
        email: String!
        avatar: String!
        is_teacher: Boolean
        is_staff: Boolean
        comments: [Comment!]!
        questions: [Question!]!
        createdAt: DateTime!
        updatedAt: DateTime!
    }
`;

module.exports.userResolvers = `
    createUser(username: String!, email: String!, password: String!, avatar: String!, is_teacher: Boolean, is_staff: Boolean): User
    deleteUser(username: String!): User!
`;

module.exports.userQueries = `
    users: [User!]!
    userDatas: User!
    user(username: String!): User!
`;