const { verify } = require('jsonwebtoken');

const isLoggedIn = async (req) => {
    try {
        let token = req.header('x-auth-token');
        if(!token) return { loggedIn: false, user: null };
        
        token = await verify(token, 'secret');
        if(token) return { loggedIn: true, user: token };

        return { loggedIn: false, user: null };

    } catch(ex) {
        throw new Error("Invalid credentials, unable to sign the user");
    }
}

const isStaff = async (req) => {
    try {
        let { loggedIn, user } = await isLoggedIn(req);
        if(!loggedIn) throw new Error("You must be logged in");

        if(!user.is_staff) throw new Error("You are not allowed to access this resource");

        return { is_staff: true }
    } catch(ex) { throw ex; }
}

const isTeacher = async (req) => {
    try {
        let { loggedIn, user } = await isLoggedIn(req);
        if(!loggedIn) throw new Error("You must be logged in");

        if(!user.is_teacher) throw new Error("You are not allowed to do this action");

        return { is_teacher: true }
    } catch(ex) { throw ex; }
}

module.exports = { isLoggedIn, isStaff, isTeacher };
