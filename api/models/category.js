const { Schema, model } = require('mongoose');
const slugify = require('slugify');
const Joi = require('joi');

const CategorySchema = new Schema({
    name: { type: String, required: true },
    slug: String,
    description: String,
    topics: Array,
}, { timestamps: true });

CategorySchema.pre('save', function() {
    this.slug = slugify(this.name);
});

const Category = model('Category', CategorySchema);

const validateCategory = (data) => Joi.validate(data, {
    name: Joi.string().min(5).required(),
    description: Joi.string().optional(),
    topics: Joi.array().optional()
});

module.exports = { Category, validateCategory };
