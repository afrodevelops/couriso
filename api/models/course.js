const { Schema, model } = require('mongoose');
const Joi = require('joi');
const slugify = require('slugify');

const CourseSchema = new Schema({
    name: { type: String, required: true },
    slug: String,
    description: String,
    topic: { type: Schema.Types.ObjectId, ref: 'Topic' },
    creators: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    students: Array,
    comments: [
        {
            user: { type: Schema.Types.ObjectId, ref: 'User' },
            content: String,
            createdAt: { type: Date, default: new Date() },
            comments: [
                {
                    user: { type: Schema.Types.ObjectId, ref: 'User' },
                    content: String,
                    createdAt: { type: Date, default: new Date() } 
                }
            ]
        }
    ],
    questions: [
        {
            user: { type: Schema.Types.ObjectId, ref: 'User' },
            content: String,
            createdAt: { type: Date, default: new Date() },
            answers: [
                {
                    user: { type: Schema.Types.ObjectId, ref: 'User' },
                    content: String,
                    createdAt: { type: Date, default: new Date() }
                }
            ]
        }
    ],
    sections: [
        {
            _id: { type: Number, required: true },
            title: { type: String, required: true },
            content: { type: String, required: true }
        }
    ]
}, { timestamps: true });

CourseSchema.pre('save', function() {
    this.slug = slugify(this.name);
});

const Course = model('Course', CourseSchema);

const validateCourse = (data) => Joi.validate(data, {
    name: Joi.string().required(),
    slug: Joi.optional(),
    description: Joi.string().optional(),
    creators: Joi.array().optional(),
    students: Joi.array().optional(),
    comments: Joi.array().optional(),
    questions: Joi.array().optional(),
    sections: Joi.array().optional(),
    topic: Joi.required()
});

module.exports = { Course, validateCourse };
