const { Schema, model } = require('mongoose');
const Joi = require('joi');
const slugify = require('slugify');

const TopicSchema = new Schema({
    name: { type: String, required: true },
    slug: String,
    description: String,
    courses: Array,
    category: { type: Schema.Types.ObjectId, ref: 'Category' }
}, { timestamps: true });

TopicSchema.pre('save', function() {
    this.slug = slugify(this.name);
});

const Topic = model('Topic', TopicSchema);

const validateTopic = (data) => Joi.validate(data, {
    name: Joi.string().required(),
    description: Joi.string().optional(),
    courses: Joi.array().optional(),
    category: Joi.required()
});

module.exports = { Topic, validateTopic };
