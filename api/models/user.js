const { Schema, model } = require('mongoose');
const Joi = require('joi');
const { hashSync, compareSync } = require('bcryptjs');

const UserSchema = new Schema({
    username: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    avatar: String,
    is_staff: { type: Boolean, default: false },
    is_teacher: { type: Boolean, default: false },
    courses: Array,
    comments: Array,
    questions: Array
}, { timestamps: true });

UserSchema.pre('save', function() {
    this.password = hashSync(this.password, 10);
});

UserSchema.methods.comparePasswords = function(password) {
    return compareSync(password, this.password);
}

const User = model('User', UserSchema);

const validateUser = (data) => Joi.validate(data, {
    username: Joi.string().min(5).required(),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    avatar: Joi.string().optional(),
    is_staff: Joi.bool().optional(),
    is_teacher: Joi.bool().optional(),
    courses: Joi.optional(),
    comments: Joi.optional(),
    questions: Joi.optional()
});

module.exports = { User, validateUser };
