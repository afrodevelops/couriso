const { createLogger, transports: { File, Console }, format: { json }} = require('winston');

const logger = createLogger({
    level: 'info',
    format: json(),
    transports: [
        new File({ filename: 'errors.log', level: 'error' }),
        new File({ filename: 'logs.log' }),
        new Console()
    ],
    exceptionHandlers: [
        new File({ filename: 'exceptions.log' })
    ]
});

module.exports = logger;