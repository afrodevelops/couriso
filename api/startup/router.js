const graphqlHttp = require('express-graphql');

const rootValue = require('../graphql/resolvers');
const schema = require('../graphql/schema');

module.exports = (app) => {
    app.use('/api', graphqlHttp({
        rootValue,
        schema,
        graphiql: true
    }));
}