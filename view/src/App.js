import React, { Component } from 'react';
import { connect } from "react-redux";
import Sign, { Signout } from "./containers/auth/sign";
import { setAuthentification } from "./actions/auth";

import { Navbar, LoginNavbar } from "./components/general";

import Courses from "./containers/courses";
import Course, { CreateCourse } from "./containers/course";

import Categories from "./containers/categories";

import Topics from "./containers/topics";
import { CreateTopic } from "./containers/topic";

import User from "./containers/auth/user";

import { LoginForbidden, TeacherRequired, LoginRequired } from "./hoc/auth";


import { Route, Switch } from "react-router-dom";
import { Layout, Row, Col } from "antd";
import "antd/dist/antd.min.css";

const { Header, Content, Footer } = Layout;

class App extends Component {
  constructor(props) {
    super(props);

    // Connexion automatique.
    if(!props.auth.isLoggedIn) {

      const token = localStorage.getItem('x-auth-token');
      if(token) {
        const isTeacher = localStorage.getItem('isTeacher');
        const isStaff = localStorage.getItem('isStaff');
        props.setAuthentification(true, isTeacher, isStaff);
      }
    }
  }
  render() {
    const { isLoggedIn, isTeacher, isStaff } = this.props.auth;
    return (
      <Layout className="layout">
        <Header>
          <Row justify='space-between'>
            <Col span={12}><Navbar /></Col>
            <Col span={12}></Col>
            <Col span={10}><LoginNavbar isLoggedIn={isLoggedIn} isTeacher={isTeacher} isStaff={isStaff} /></Col>
          </Row>
        </Header>
        <Content style={{ padding: '0 30px' }}>
          <section style={{ background: '#fff', padding: 24, margin: 24, minHeight: '70vh' }}>
            <Switch>
              <Route path="/sign" exact strict component={LoginForbidden(Sign)} />
              <Route path="/signout" exact strict component={Signout} />
              <Route path="/me" exact strict component={LoginRequired(User)} />

              <Route path="/courses" exact strict component={Courses} />
              <Route path="/course/:slug" exact strict component={Course} />
              <Route path="/add/course" exact strict component={TeacherRequired(CreateCourse)} />

              <Route path="/categories" exact strict component={Categories} />

              <Route path="/topics" exact strict component={Topics} />
              <Route path="/add/topic" exact strict component={TeacherRequired(CreateTopic)} />
            </Switch>
          </section>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          Created using ReactV16, GraphQL and MongoDB ©2019 By AfroDevelops
        </Footer>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setAuthentification: (isLoggedIn, isTeacher, isStaff) => dispatch(setAuthentification(isLoggedIn, isTeacher, isStaff)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
