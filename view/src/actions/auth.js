import { post } from "axios";
import { API_URL } from "../config";
import { SIGN_IN, SET_AUTHENTICATION } from "./action-types";

export const signIn = ({ email, password }) => {
    return {
        type: SIGN_IN,
        payload: new Promise(async (resolve, reject) => {
            try {
                const { data: { data } } = await post(API_URL, {
                    query: `
                        query {
                            authenticate(email: "${email}", password: "${password}") {
                                token
                                is_teacher
                                is_staff
                            }
                        }
                    `
                });
                const { authenticate: { token, is_teacher, is_staff }} = data;
                resolve({ token, is_teacher, is_staff });

            } catch(ex) {
                reject(ex);
            }
        })
    }
}

export const setAuthentification = (isAuthenticated, isTeacher, isStaff) => {
    return { type: SET_AUTHENTICATION, payload: { isLoggedIn: isAuthenticated, isTeacher, isStaff } };
}