import { CATEGORIES } from "./action-types";
import { post } from "axios";
import { API_URL } from "../config";

export const getCategories = () => {
    return {
        type: CATEGORIES,
        payload: new Promise(async (resolve, reject) => {
            try {
                const { data: { data }} = await post(API_URL, {
                    query: `
                        query {
                            categories {
                                _id
                                slug
                                name
                                description
                            }
                        }
                    `
                });

                const { categories } = data;
                resolve(categories);
            } catch(ex) {   
                reject(ex);
            }
        })
    }
}

export const selectCategories = () => {
    return {
        type: CATEGORIES,
        payload: new Promise(async (resolve, reject) => {
            try {
                const { data: { data }} = await post(API_URL, {
                    query: `
                        query {
                            categories {
                                _id
                                name
                            }
                        }
                    `
                });

                const { categories } = data;
                resolve(categories);
            } catch(ex) {   
                reject(ex);
            }
        })
    }
}