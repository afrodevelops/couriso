import { post } from "axios";
import { API_URL } from "../config";
import { COURSES, COURSE, ADD_COURSE } from "./action-types";

export const getCourse = (slug) => {
    return {
        type: COURSE,
        payload: new Promise(async (resolve, reject) => {
            try {
                const { data: { data }} = await post(API_URL, {
                    query: `
                        query {
                            course(slug: "${slug}") {
                                _id
                                name
                                description
                                questions {
                                    user {
                                        username
                                    }
                                    content
                                    createdAt
                                    answers { user { username } content createdAt }
                                }
                                comments {
                                    user { username }
                                    content
                                    createdAt
                                    comments { user { username } content createdAt }
                                }
                                sections {
                                    title
                                    content
                                }
                                createdAt
                                updatedAt
                                creators {
                                    username
                                    is_teacher
                                }
                                topic {
                                    name
                                    slug
                                }
                            }
                        }
                    `
                });

                const { course } = data;
                resolve(course);
            } catch(ex) { reject(ex); }
        })
    }
}

export const getCourses = () => {
    return {
        type: COURSES,
        payload: new Promise(async (resolve, reject) => {
            try {
                const { data: { data } } = await post(API_URL, {
                    query: `
                        query {
                            coursesByTopic {
                                topic {
                                    name
                                    slug
                                }
                                courses {
                                    _id
                                    name
                                    slug
                                    description
                                }
                            }
                        }
                    `
                });

                console.log(data)

                resolve(data.coursesByTopic);

            } catch(ex) {
                reject(ex);
            }
        })
    }
}

export const createCourse = ({ name, description, topic }) => {
    return {
        type: ADD_COURSE,
        payload: new Promise(async (resolve, reject) => {
            try {
                const { data: { data }} = await post(API_URL, {
                    query: `
                        mutation {
                            createCourse(name: "${name}", description: "${description}", topic: "${topic}") {
                                _id
                            }
                        }
                    `
                }, { headers: { 'x-auth-token': localStorage.getItem('x-auth-token') }});

                resolve(data);
            } catch(ex) { reject(ex); }
        })
    }
}