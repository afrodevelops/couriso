import { TOPICS, CREATE_TOPIC } from "./action-types";
import { post } from "axios";
import { API_URL } from "../config";

export const getTopics = () => {
    return {
        type: TOPICS,
        payload: new Promise(async (resolve, reject) => {
            try {
                const { data: { data }} = await post(API_URL, {
                    query: `
                        query {
                            topics {
                                _id
                                name
                                slug
                                description
                            }
                        }
                    `
                });

                const { topics } = data;
                resolve(topics);
            } catch(ex) {
                reject(ex);
            }
        })
    }
}

export const createTopic = ({ name, description, category }) => {
    return {
        type: CREATE_TOPIC,
        payload: new Promise(async (resolve, reject) => {
            try {
                const { data: { data } } = post(API_URL, {
                    query: `
                        mutation {
                            createTopic(name: "${name}", description: "${description}", category: "${category}") {
                                _id
                            }
                        }
                    `
                }, {
                    headers: { 'x-auth-token': localStorage.getItem('x-auth-token') }
                });

                resolve(data);
            } catch(ex) {reject(ex); }
        })
    }
}