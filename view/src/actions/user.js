import { GET_USER } from "./action-types";
import { post } from "axios";
import { API_URL } from "../config";

export const getUserDatas = async () => {
    return {
        type: GET_USER,
        payload: new Promise(async (resolve, reject) => {
            try {
                const { data: { data }} = await post(API_URL, {
                    query: `
                        query {
                            userDatas {
                                _id
                                username
                                email
                                createdAt
                            }
                        }
                    `
                }, { headers: { 'x-auth-token': localStorage.getItem('x-auth-token') }});

                resolve(data.userDatas);
            } catch(ex) { reject(ex); }
        })
    }
}