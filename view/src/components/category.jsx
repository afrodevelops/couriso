import React from "react";
import { Link } from "react-router-dom";

export const CategorySnapshot = ({ _id, name, slug, description }) => {
    return (
        <div>
            <h3>{ name }</h3>
            <p>{ description }</p>
            <div>
                <Link to="/">More details</Link>
            </div>
        </div>
    );
}
