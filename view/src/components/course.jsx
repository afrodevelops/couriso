import React from "react";
import { Link } from "react-router-dom";
import { Row, Col, Empty, Card } from "antd";

export const CourseSnapshot = ({ _id, slug, name, description }) => {
    return (
        <Card title={name}>
            <div>
                <p>{ description }</p>
            </div>
            <div>
                <a href={`/course/${slug}`}>More details</a>
            </div>
        </Card>
    );
};

export const CourseDetail = ({ _id, name, slug, description, createdAt, updatedAt, creators, topic, sections, comments, questions }) => {
    return (
        <Row>
            <Col span={4}>
                <div>
                    <h3 className="uk-margin-bottom">{ name }</h3>
                    <p className="uk-text-meta">Cours ajouté le { createdAt }</p>
                    <p>{ description }</p>
                    <hr/>

                    <h3 className="uk-text-bold">Creators</h3>
                    { creators.map((creator, key) => {
                        const { username } = creator;
                        return (
                            <div key={key} className="uk-card uk-card-default uk-card-body uk-margin-bottom uk-padding-small">
                                <span className="uk-text-meta uk-text-bold">
                                    <span uk-icon="icon:happy"></span>
                                    { username }
                                </span>
                            </div>
                        );
                    })}
                </div>
            </Col>
            <Col span={8}>
                <div>
                    { sections.length === 0 && (
                        <Empty description={
                            <span>
                                <p>No section has been added yet.</p>
                            </span>
                        } style={{ margin: 'auto' }} />
                    ) }
                </div>
            </Col>
        </Row>
    );
};
