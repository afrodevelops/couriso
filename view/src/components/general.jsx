import React from "react";
import { Link } from "react-router-dom";
import { Menu, Icon, Dropdown } from "antd";

const teacherActions = (
    <Menu>
        <Menu.Item>
            <Link to="/me/activity#courses">My courses</Link>
        </Menu.Item>
    </Menu>
);

export const LoginNavbar = (props) => {
    const { isLoggedIn, isTeacher, isStaff } = props;

    return !isLoggedIn ? (
            <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={['0']}
                style={{ lineHeight: '64px' }}
            >
                <Menu.Item>
                    <Link to="/sign">Sign in</Link>
                </Menu.Item>
                <Menu.Item>
                    <Link to="/signup">Sign up</Link>
                </Menu.Item>
            </Menu>
        ):  (
            <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={['0']}
                style={{ lineHeight: '64px' }}
            >
                <Menu.Item>
                    <Link to="/signout">Sign out</Link>
                </Menu.Item>
                <Menu.Item>
                    {
                        (isTeacher) ? (
                            <Dropdown overlay={teacherActions}>
                                <Link to="/me">My account</Link>
                            </Dropdown>
                        ) : <Link to="/me">My account</Link>
                    }
                </Menu.Item>
                {
                    (isTeacher || isStaff) && (
                        <Menu.SubMenu title={<Icon type="plus" />}>
                            <Menu.Item>
                                <Link to="/add/topic">Add topic</Link>
                            </Menu.Item>
                            { (isTeacher) && (
                                <Menu.Item>
                                    <Link to="/add/course">Add course</Link>
                                </Menu.Item>
                            ) }

                            { (isStaff) && (
                                    <Menu.Item>
                                        <Link to="/">Manage</Link>
                                    </Menu.Item>
                                )
                            }
                        </Menu.SubMenu>
                    )
                }
            </Menu>
    );
}

export const Navbar = (props) => {
    return (
        <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={['0']}
            style={{ lineHeight: '64px' }}
        >
            <Menu.Item key="0">
                <Link to="/"><Icon type="home" /></Link>
            </Menu.Item>
            <Menu.Item key="1">
                <Link to="/categories">Categories</Link>
            </Menu.Item>
            <Menu.Item key="2">
                <Link to="/topics">Topics</Link>
            </Menu.Item>
            <Menu.Item key="3">
                <Link to="/courses">Courses</Link>
            </Menu.Item>
      </Menu>
    );
}