import React from "react";
import { Link } from "react-router-dom";

export const TopicSnapshot = ({ _id, name, slug, description }) => {
    return (
        <div>
            <div>
                <h3>{ name }</h3>
            </div>
            <div>
                <p>{ description }</p>
            </div>
            <div>
                <Link to="/">More details</Link>
            </div>
        </div>
    );
};
