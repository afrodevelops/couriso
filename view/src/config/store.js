import { createStore, applyMiddleware, compose } from "redux";
import promise from "redux-promise-middleware";

import reducers from "../reducers";

const store = createStore(
    reducers,
    {},
    compose(
        applyMiddleware(
            promise,
        ),
        window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
    )
);

export default store;
