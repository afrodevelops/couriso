import React from 'react';
import { connect } from 'react-redux';
import { signIn, setAuthentification } from "../../actions/auth";
import { Link } from "react-router-dom";

import {
    Form, Icon, Input, Button, Checkbox,
  } from 'antd';

class Logout extends React.Component {
    constructor(props) {
        super(props);
        if(props.auth.isLoggedIn) {
            localStorage.removeItem('x-auth-token');
            localStorage.removeItem('isTeacher');
            localStorage.removeItem('isStaff');
            
            props.setAuthentification(false, false, false);
            props.history.push('/');
        }
    }
    render() {
        return "";
    }
}

class Sign extends React.Component {
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields(async (err, data) => {
        if (!err) {
            const { value: { token, is_teacher, is_staff } } = await this.props.signIn(data);

            localStorage.setItem('x-auth-token', token);
            localStorage.setItem('isTeacher', is_teacher);
            localStorage.setItem('isStaff', is_staff);

            this.props.setAuthentification(true, is_teacher, is_staff);
        }
      });
    }
  
    render() {
      const { getFieldDecorator } = this.props.form;
      return (
        <Form onSubmit={this.handleSubmit} className="login-form">
          <Form.Item>
            {getFieldDecorator('email', {
              rules: [{ required: true, message: 'Please enter your email address' }],
            })(
              <Input
                prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                type="email" placeholder="Email"
            />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Please enter your Password!' }],
            })(
              <Input
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                type="password"
                placeholder="Password"
            />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: true,
            })(
              <Checkbox>Remember me</Checkbox>
            )}
            <a className="login-form-forgot" href="">Forgot password</a>
            <Button type="primary" htmlType="submit" className="login-form-button">
              Log in
            </Button>
            Or <Link to="/signup">register now!</Link>
          </Form.Item>
        </Form>
      );
    }
  }
  
const WrappedSign = Form.create({ name: 'login' })(Sign);

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        signIn: async (data) => dispatch(await signIn(data)),
        setAuthentification: (isAuthenticated, isTeacher, isStaff) => dispatch(setAuthentification(isAuthenticated, isTeacher, isStaff)),
    };
}

export const Signout = connect(mapStateToProps, mapDispatchToProps)(Logout);
export default connect(mapStateToProps, mapDispatchToProps)(WrappedSign);

