import React from 'react';
import { connect } from 'react-redux';
import { Layout, Menu, Icon, Empty, Anchor, Tabs } from "antd";

import { UserPersonalDatas, UserSettings } from "../../forms/user";

const { Content, Sider } = Layout;
const { SubMenu } = Menu;
const { Link } = Anchor;

const UserPersonnal = (props) => {
    return (
        <div>
            <div style={{ margin: '2%'}} id="personal-datas">
                    <h2>Personnal datas</h2>
                    <UserPersonalDatas />
                </div>
                <div style={{ margin: '2%'}} id="settings">
                    <h2>Settings</h2>
                    <UserSettings />
                </div>
                <div style={{ margin: '2%'}} id="activities">
                    <h2>My activity</h2>
                    <Empty description="No activity yet" />
                </div>
                <div style={{ margin: '2%'}} id="notifications">
                    <h2>Notifications</h2>
                    <Empty description="No notifications yet" />
                </div>
        </div>
    );
}

const UserActivity = (props) => {
    return "Activty";
}

class User extends React.Component {
    constructor(props) {
        super(props);
        this.isTeacher = true;
    }
    renderUserInterface = () => {
        return (
            <Layout style={{ padding: '24px 0', background: '#fff' }}>
            <Sider width={200} style={{ background: '#fff' }}>
            <Menu
                mode="inline"
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                style={{ height: '100%' }}
            >
                <SubMenu key="sub1" title={<span><Icon type="user" />Me</span>}>
                <Anchor>
                    <Link href="#personal-datas" title="Personnal datas"/>
                    <Link href="#settings" title="Settings"/>
                    <Link href="#activities" title="Activity"/>
                    <Link href="#notifications" title="Notification" />
                </Anchor>
                </SubMenu>
                { this.props.isTeacher && (
                    <SubMenu key="sub2" title={<span><Icon type="laptop" />Activity</span>}>
                    <Anchor>
                    <Link href="#my-courses" title="My courses" />
                    <Link href="#new-comments" title="New comments" />
                    <Link href="#new-questions" title="New questions" />
                    </Anchor>
                    </SubMenu>
                )}
            </Menu>
            </Sider>
            <Content style={{ padding: '0 24px', minHeight: 280 }}>
                <Tabs defaultActiveKey="0">
                    <Tabs.TabPane key="0" tab={<span><Icon type="user" />General informations</span>}>
                        <UserPersonnal />
                    </Tabs.TabPane>
                    <Tabs.TabPane key="1" tab={<span><Icon type="rise" />Activity</span>}>
                        <UserActivity />
                    </Tabs.TabPane>
                </Tabs>
            </Content>
        </Layout>
        );
    }
    render = () => {
        return <div>{ this.renderUserInterface() }</div>;
    }
}
const mapStateToProps = (state) => {
    return {
        isTeacher: state.auth.isTeacher,
        isStaff: state.auth.isStaff,
    };
}
const mapDispatchToProps = (dispatch) => {
    return {};
}
export default connect(mapStateToProps, mapDispatchToProps)(User)
