import React from 'react';
import { connect } from 'react-redux';

import { getCategories } from "../actions/category";
import { categories } from "../selectors/category";
import { CategorySnapshot } from "../components/category";

class Categories extends React.Component {
    constructor(props) {
        super(props);
        props.getCategories();
    }
    renderCategories = () => {
        const categories = this.props.categories.map((category, key) => {
            const { _id, name, description } = category;
            return (
                <div key={key}>
                    <CategorySnapshot _id={_id} name={name} description={description} />
                </div>
            );
        });
        return categories;
    }
    render(){

        return (
            <div>
                <div>
                    { this.renderCategories() }
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        categories: categories(state),
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        getCategories: async () => dispatch(await getCategories()),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Categories);
