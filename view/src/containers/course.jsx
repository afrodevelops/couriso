import React from 'react';
import { connect } from 'react-redux';

import { getCourse, createCourse } from "../actions/course";
import { getTopics } from "../actions/topic";

import { CourseDetail } from "../components/course";
import { course } from "../selectors/course";

import { Form, Input, Button, Select, notification, Icon } from "antd";

class Create extends React.Component {
    constructor(props) {
        super(props);
        props.getTopics();

    }
    handleSubmit = async (e) => {
        e.preventDefault();
        this.props.form.validateFields(async (err, data) => {
            if(!err) {
                const { value } = await this.props.createCourse(data);
                if(value) {
                    notification.open({
                        message: 'Added successfully',
                        description: 'Your course has been successfully added',
                        icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
                    });
                }
            }
        })
    }
    renderForm = () => {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Item label="Title">
                    {getFieldDecorator('name', {
                        rules: [{ required: true, message: 'Enter a name for your course' }]
                    })(
                        <Input placeholder="Title of the course" />
                    )}

                </Form.Item>
                <Form.Item label="Description">
                    {
                        getFieldDecorator('description', {
                            rules: [{ required: true, message: 'Enter the content of your course' }]
                        })(
                            <Input.TextArea placeholder="A description of your course" />
                        )
                    }
                </Form.Item>
                <Form.Item label="Topic of your course">
                    {
                        getFieldDecorator('topic', {
                            rules: [{ required: true, message: 'Your course must have one topic' }]
                        })(
                            <Select>
                                {this.props.topics.map((topic, key) => {
                                    return (
                                        <Select.Option key={key} value={topic._id}>
                                            { topic.name}
                                        </Select.Option>
                                    )
                                })}
                            </Select>
                        )
                    }
                </Form.Item>
                <Button type="primary" htmlType="submit" className="login-form-button">Add</Button>
            </Form>
        );
    }
    render() {
        return (
            <div>
                <div>{ this.renderForm() }</div>
            </div>
        );
    }
}

Create = Form.create({ name: 'create-course'})(Create);
const createStateToProps = (state) => {
    return {
        topics: state.topic.topics,
    }
}
const createDispatchToProps = (dispatch) => {
    return {
        createCourse: async (data) => dispatch(await createCourse(data)),
        getTopics: async () => dispatch(await getTopics()),
    }
}

class Course extends React.Component {
    constructor(props) {
        super(props);
        props.getCourse(props.match.params.slug);
    }
    renderCourse = () => {
        const { course } = this.props;
        if(!course) return <p>Chargement</p>

        const { name, _id, description, slug, creators, createdAt, updatedAt, topic, questions, comments, sections } = course;

        return (
            <div>
                <CourseDetail
                    name={name} _id={_id}
                    description={description} slug={slug} creators={creators}
                    createdAt={createdAt} updatedAt={updatedAt} topic={topic}
                    questions={questions} comments={comments} sections={sections}
                />
            </div>
        );
    }
    render() {
        return (
            <div>
                { this.renderCourse() }
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        course: course(state),
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        getCourse: async (slug) => dispatch(await getCourse(slug)),
    };
}

export const CreateCourse = connect(createStateToProps, createDispatchToProps)(Create);
export default connect(mapStateToProps, mapDispatchToProps)(Course);
