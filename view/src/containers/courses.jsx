import React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Divider } from "antd";
import { Link } from "react-router-dom";

import { courses } from "../selectors/course";
import { getCourses } from "../actions/course";
import { CourseSnapshot } from "../components/course";

class Courses extends React.Component {
    constructor(props) {
        super(props);
        props.getCourses();
    }
    renderCourses = () => {
        console.log(this.props)
        let { courses } = this.props;
        if(!courses) return <p>Chargement des cours en cours</p>;

        courses = courses.map((course, key) => {
             
            const renderHTML = course.courses.length > 0 ? (
                <div style={{ overflow: 'hidden' }}>
                    <Divider orientation="left"><Link to="">{ course.topic.name }</Link></Divider>
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        {
                            course.courses.map(({ name, slug, description, _id}, k) => (
                                    <Col xs={24} sm={12} md={8} span={8} key={k}>
                                        <CourseSnapshot key={key} name={name} slug={slug} description={description} _id={_id} />
                                    </Col>
                                )
                            )
                        }
                    </Row>
                </div>
            ): "";
            return renderHTML;
        });
        return courses;
    }
    render(){
        return <div>{ this.renderCourses() }</div>;
    }
}
const mapStateToProps = (state) => {
    return {
        courses: courses(state),
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        getCourses: async () => dispatch(await getCourses()),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Courses)
