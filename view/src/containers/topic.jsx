import React from "react";
import { connect } from "react-redux";
import { Form, Input, notification, Icon, Button, Select } from "antd";
import { createTopic } from "../actions/topic";
import { selectCategories } from "../actions/category";

class Create extends React.Component {
    constructor(props) {
        super(props);
        props.selectCategories();
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields(async (err, data) => {
            if(!err) {
                const { value } = await this.props.createTopic(data);
                if(value) {
                    notification.open({
                        description: 'The topic has been successfully added',
                        message: 'Added successfully',
                        icon: <Icon type="smile" style={{ color: '#108ee9' }} />
                    })
                }
            }
        });
    }
    renderForm = () => {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Item label="Title">
                    {
                        getFieldDecorator('name', {
                            rules: [{ required: true, message: 'Please enter a title for your topic' }]
                        })(
                            <Input placeholder="Title of the topic" />
                        )
                    }
                </Form.Item>
                <Form.Item label="Description">
                    {
                        getFieldDecorator('description', {
                            rules: [{ required: true, message: 'Please enter a description for your topic' }]
                        })(
                            <Input.TextArea placeholder="Description of the topic" />
                        )
                    }
                </Form.Item>
                <Form.Item label="Category">
                    {
                        getFieldDecorator('category', { rules: [
                            { required: true, message: 'A topic needs a category' }
                        ]})(
                            <Select>
                                { this.props.categories.map((c, k) => (
                                    <Select.Option key={k} value={c._id}>
                                        { c.name }
                                    </Select.Option>
                                ))}
                            </Select>
                        )
                    }
                </Form.Item>
                <Button type="primary" htmlType="submit" className="login-form-button">Add</Button>
            </Form>
        );
    }
    render() {
        return (
            <div>
                <h3>Add a new topic</h3>
                <div>
                    { this.renderForm() }
                </div>
            </div>
        );
    }
}

Create = Form.create({ name: 'create-topic' })(Create);
const createStateToProps = (state) => {
    return { categories: state.category.categories };
}
const createDispatchToProps = (dispatch) => {
    return { createTopic: async (data) => dispatch(await createTopic(data)), selectCategories: async () => dispatch(await selectCategories()), };
}

export const CreateTopic = connect(createStateToProps, createDispatchToProps)(Create);
