import React from 'react';
import { connect } from 'react-redux';

import { getTopics } from "../actions/topic";
import { topics } from "../selectors/topic";
import { TopicSnapshot } from "../components/topic";

class Topics extends React.Component {
    constructor(props) {
        super(props);
        props.getTopics();
    }
    renderTopics = () => {
        let { topics } = this.props;
        topics = topics.map((topic, key) => {
            const { _id, name, slug, description } = topic;
            return (
                <div key={key}>
                    <TopicSnapshot _id={_id} name={name} slug={slug} description={description} />
                </div>
            );
        });
        return topics;
    }
    render(){
        return (
            <div>
                <div>
                    { this.renderTopics() }
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        topics: topics(state),
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        getTopics: async () => dispatch(await getTopics()),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Topics)
