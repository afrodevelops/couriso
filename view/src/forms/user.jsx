import React from "react";
import { connect } from "react-redux";
import { Form, Input, Row, Col, Popconfirm, Button } from "antd";

import { getUserDatas } from "../actions/user";

class RenderUserPersonalDatas extends React.Component {
    constructor(props) {
        super(props);
        props.getUserDatas();
    }
    handleSubmit = (e) => {

    }
    render() {
        const { form: { getFieldDecorator }, _id, username, email, createdAt } = this.props;
        return (
            <Form onSubmit={this.handleSubmit} layout='horizontal'>
                <Row gutter={6}>
                    <Col xs={24} sm={12} md={12}>
                        <Form.Item label="Your ID">
                            {
                                getFieldDecorator('_id', { initialValue: _id, disabled: true })(
                                    <Input disabled="true" />
                                )
                            }
                        </Form.Item>
                    </Col>
                    <Col xs={24} sm={12} md={12}>
                        <Form.Item label="Username">
                            {
                                getFieldDecorator('username', { initialValue: username })(
                                    <Input />
                                )
                            }
                        </Form.Item>
                    </Col>
                    <Col xs={24} sm={12} md={12}>
                        <Form.Item label="Email address">
                            {
                                getFieldDecorator('email', { initialValue: email })(
                                    <Input />
                                )
                            }
                        </Form.Item>
                    </Col>
                    <Col xs={24} sm={12} md={12}>
                        <Form.Item label="Created at">
                            {
                                getFieldDecorator('createdAt', { initialValue: createdAt })(
                                    <Input disabled="true" />
                                )
                            }
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        );
    }
}

const mapStateToProps = (state) => {
    const { _id, email, username, createdAt } = state.user.user;
    return { _id, email, username, createdAt };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getUserDatas: async () => dispatch(await getUserDatas()),
    }
}

RenderUserPersonalDatas = Form.create({ name: 'user-personal-datas' })(RenderUserPersonalDatas);
export const UserPersonalDatas = connect(mapStateToProps, mapDispatchToProps)(RenderUserPersonalDatas);

class RenderUserSettings extends React.Component {
    constructor(props) {
        super(props);
    }
    handleChangePassword = (e) => {
        e.preventDefault();
    }
    renderUserSettings = () => {
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <Popconfirm okText="Yes" cancelText="Cancel" style={{ margin: "2%" }}>
                    <p style={{ padding: '2%' }}>
                        Deleting your account will not delete it in our database in order to keep consistency in it.
                        But you will not be able to access it.
                        All your activity will be hidden.
                    </p>
                    <Button type="danger">Delete my account</Button>
                </Popconfirm>
                <div style={{ margin: "2%" }}>
                    <h3 style={{ fontWeight: "bold" }}>Change your password</h3>
                    <Form onSubmit={this.handleChangePassword}>
                        <Row gutter={4}>
                            <Col xs={24}>
                                <Form.Item label="Current password">
                                    {
                                        getFieldDecorator('password', { rules: [
                                            { required: true, message: "You must enter your current password to change it" }
                                        ]})(
                                            <Input type="password" />
                                        )
                                    }
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12}>
                                <Form.Item label="New password">
                                    {
                                        getFieldDecorator('newPassword', { rules: [
                                            { required: true, message: "You must enter the new password" }
                                        ]})(
                                            <Input type="password" />
                                        )
                                    }
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12}>
                            <Form.Item label="Repeat the new password">
                                {
                                    getFieldDecorator('password', { rules: [
                                        { required: true, message: "You must repeat password" }
                                    ]})(
                                        <Input type="password" />
                                    )
                                }
                            </Form.Item>
                        </Col>
                        </Row>
                    </Form>
                </div>
            </div>
        );
    }
    render() {
        return <div>{ this.renderUserSettings() }</div>;
    }
}

const settingsDispatchToProps = (dispatch) => {}

RenderUserSettings = Form.create({ name: 'user-settings'})(RenderUserSettings);
export const UserSettings = connect(mapStateToProps, settingsDispatchToProps)(RenderUserSettings);
