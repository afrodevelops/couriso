import React from 'react';
import { connect } from "react-redux";

export const LoginRequired = (Component) => {
    class HOC extends React.Component {
        constructor(props) {
            super(props);
            if(!props.auth.isLoggedIn) props.history.push('/sign');
        }
        render() {
            return <Component />
        }
    }

    const mapStateToProps = (state) => {
        return {
            auth: state.auth,
        }
    }

    return connect(mapStateToProps, null)(HOC);
}

export const LoginForbidden = (Component) => {
    class HOC extends React.Component {
        constructor(props) {
            super(props);
            if(props.auth.isLoggedIn) props.history.push('/');
        }
        render() {
            return <Component />
        }
    }

    const mapStateToProps = (state) => {
        return {
            auth: state.auth,
        }
    }

    return connect(mapStateToProps, null)(HOC);
}

export const TeacherRequired = (Component) => {
    class HOC extends React.Component {
        constructor(props) {
            super(props);
            if(!props.auth.isTeacher) props.history.push('/');
        }
        render() {
            return <Component />
        }
    }

    const mapStateToProps = (state) => {
        return {
            auth: state.auth,
        }
    }

    return connect(mapStateToProps, null)(HOC);
}
