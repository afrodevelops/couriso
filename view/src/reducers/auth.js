import { SIGN_IN_FULFILLED, SET_AUTHENTICATION } from "../actions/action-types";

const initialState = {
    token: "",
    isLoggedIn: false
};

export const auth = (state = initialState, action) => {
    switch(action.type) {
        case SIGN_IN_FULFILLED:
            const { token } = action.payload;
            state = { ...state, token };
            break;

        case SET_AUTHENTICATION:
            const { isLoggedIn, isTeacher, isStaff } = action.payload;
            state = { ...state, isLoggedIn, isTeacher, isStaff };
            break;
    }
    return state;
}
