import { CATEGORIES_FULFILLED } from "../actions/action-types";

const initialState = {
    categories: []
};

export const category = (state=initialState, action) => {
    switch(action.type) {
        case CATEGORIES_FULFILLED:
            state = { ...state, categories: action.payload };
            break;
    }
    return state;
}
