import { COURSES_FULFILLED, COURSE_FULFILLED, ADD_COURSE_FULFILLED } from "../actions/action-types";

const initialState = {
    courses: [],
};

export const course= (state = initialState, action) => {
    switch(action.type) {
        case COURSES_FULFILLED:
            state = { ...state, courses: action.payload };
            break;

        case COURSE_FULFILLED:
            state = { ...state, course: action.payload };
            break;

        case ADD_COURSE_FULFILLED:
            state = { ...state, course: action.payload };
            break;
    }
    return state;
}
