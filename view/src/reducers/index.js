import { combineReducers } from "redux";

import { course } from "./course";
import { category } from "./category";
import { topic } from "./topic";
import { auth } from "./auth";
import { user } from "./user";

const reducers = combineReducers({
    course,
    category,
    topic,
    user,
    auth
});

export default reducers;
