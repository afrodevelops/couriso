import { TOPICS_FULFILLED, CREATE_TOPIC_FULFILLED } from "../actions/action-types";

const initialState = {
    topics: []
};

export const topic = (state=initialState, action) => {
    switch(action.type) {
        case TOPICS_FULFILLED:
            state = { ...state, topics: action.payload };
            break;

        case CREATE_TOPIC_FULFILLED:
            state = { ...state, topic: action.payload };
            break;
    }

    return state;
}

