import { GET_USER_FULFILLED } from "../actions/action-types";

const initialState = { user: {}};

export const user = (state=initialState, action) => {
    switch(action.type) {
        case GET_USER_FULFILLED:
            state = { ...state, user: action.payload };
            break;
    }

    return state;
}