export const courses = (state) => state.course.courses;

export const course = (state) => state.course.course;
